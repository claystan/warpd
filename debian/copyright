Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: warpd
Upstream-Contact: https://github.com/rvaiya/warpd/issues
Source: https://github.com/rvaiya/warpd

Files: *
Copyright: 2019-2022 Raheman Vaiya <r.vaiya@gmail.com>
License: MIT

Files: src/platform/wayland/wl/layer-shell.h
       src/platform/wayland/wl/layer-shell.c
Copyright: 2017 Drew DeVault
License: ISC

Files: src/platform/wayland/wl/virtual-pointer.h
       src/platform/wayland/wl/xdg-output.h
       src/platform/wayland/wl/xdg-shell.h
       src/platform/wayland/wl/virtual-pointer.c
       src/platform/wayland/wl/xdg-output.c
       src/platform/wayland/wl/xdg-shell.c
Copyright: 2008-2013 Kristian Høgsberg
           2010-2013 Intel Corporation
           2013 Jasper St. Pierre
           2013 Rafael Antognolli
           2015-2017 Red Hat Inc.
           2015-2017 Samsung Electronics Co., Ltd
           2019 Josef Gajdusek
License: MIT

Files: debian/*
Copyright: 2022 Clay Stan <claystan97@gmail.com>
           2019-2022 Raheman Vaiya <r.vaiya@gmail.com>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: ISC
 Permission to use, copy, modify, distribute, and sell this
 software and its documentation for any purpose is hereby granted
 without fee, provided that the above copyright notice appear in
 all copies and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of
 the copyright holders not be used in advertising or publicity
 pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no
 representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied
 warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 THIS SOFTWARE.
